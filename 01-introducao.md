#Introdução

#Conteúdo
- Javascript
- Desenvolvimento Orientado a Teste (TDD)
  - Conceito
  - Jest
- Análise de Dados
  - Pré-processamento
  - Modelagem de dados
  - Análise de dados
- Inteligência Artificial
  - Algoritmo KNN
  - Algoritmos Genéticos
  - Algoritmos de Árvore de Decisão
