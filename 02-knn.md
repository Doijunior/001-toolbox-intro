#Por que?
para que o KNN é um método utilizável?
resolver problemas de classificação e regressão de dados que apresentam características de agrupamento forte.
Exemplo: apresentar caso da iris


#O que?
identificar os k vizinhos mais próximos do ponto que queremos prever:
  - calcular distancia entre dois pontos
  - percorrer o conjunto de trainamento
  - manter os dados dos k vizinhos mais próximos
  - realizar a predição com base nas classes/valores dos k vizinhos

#Como?
