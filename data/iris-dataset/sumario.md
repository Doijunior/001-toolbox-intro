#Conjunto de dados 1: Iris

O Conjunto criado e disponibilizado por R.A. Fisher em Julho de 1988 é amplamente conhecido e utilizado em estudos de análise de dados. Contém dados de 150 exemplares, sendo 50 de cada uma das subclasses representadas.

Iris é um gênero de plantas com flor que apresenta subclassificações de acordo com algumas características. Esse conjunto de dados apresenta dados de três subclasses de Iris:
- Iris Setosa
- Iris Versicolour
- Iris Virginica

Cada exemplar apresenta dados de 5 atributos:
- altura da sepala em cm
- largura da sepala em cm
- altura da petala em cm
- largura da petala em cm
- classe
